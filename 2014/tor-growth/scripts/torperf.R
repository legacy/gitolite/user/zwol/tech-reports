rm( list = ls() )
setwd('/Users/know/Desktop/tor analytics/')
library(car)    # for pretty plots
library(matlab) # for matlab function names
library(data.table) # for data.table like data.frame
#library(xtable) # for exporting to LaTeX
#library(gdata)
library(lmtest) # for testing linear models
library(calibrate)

library(plyr) # for renaming columns
source("colortitles.R")
#######################################################

process_torperf_rawdata <- function( filename, filesize_to_consider=5242880 )
{
  
  # Import data from TorPerf
  Dtp <- read.csv( filename )[c('day','date','size','source','q1','md','q3')]
  Dtp <- rename(Dtp, c("size"="filesize") )
  
  print(unique(Dtp$filesize))
  # only use the aggregated Tor data for downloading a 5 MiB file
  Dtp <- subset( Dtp, source=='' & filesize==filesize_to_consider )
  
  #print( tail(Dtp) )
  
  # drop the source and filesize column
  #Dtp <- Dtp[ , -which(names(Dtp) %in% c('source','filesize'))]
  Dtp <- Dtp[ , -which(names(Dtp) %in% c('source'))]
  
  # rename the q1, md, and q3 for TIME
  Dtp <- rename(Dtp, c("q1"="time_q1","md"="time_md","q3"="time_q3") )
  
  # convert time from MILLISECONDS -> SECONDS
  Dtp$time_q1 <- Dtp$time_q1/1000
  Dtp$time_md <- Dtp$time_md/1000
  Dtp$time_q3 <- Dtp$time_q3/1000
  
  
  # now create the bw_q1, bw_md, bw_q3 in: KiB/s
  Dtp[c("bw_q1","bw_md","bw_q3")] <- c(NA,NA,NA)
  
  #convert my_filesize to 
  
  # Rewrite q1, md, and q3 to be in bandwidth (KiB/s)
  Dtp$bw_q1 <- (filesize_to_consider / 1024) / Dtp$time_q1;
  Dtp$bw_md <- (filesize_to_consider / 1024) / Dtp$time_md;
  Dtp$bw_q3 <- (filesize_to_consider / 1024) / Dtp$time_q3;
  
  return(Dtp)
}

# get the two groups
D_BIG <- process_torperf_rawdata('torperf-clean.csv', 5242880)
D_MED <- process_torperf_rawdata('torperf-clean.csv', 1048576)
D_SMALL <- process_torperf_rawdata('torperf-clean.csv', 51200)


# DO ANALYSIS FROM HERE
#######################################################

plot(D_BIG$day, log2(D_BIG$bw_md), xlab='Year', ylab="Torperf bandwidth (KiB/s)", pch='', xaxt='n', yaxt='n', ylim=c(2.5,9) )
#points(D_BIG$day, log2(D_BIG$bw_md), pch=20, col='blue', cex=0.4 )
#points(D_MED$day, log2(D_MED$bw_md), pch=20, col='purple', cex=0.4 )
#points(D_SMALL$day, log2(D_SMALL$bw_md), pch=20, col='red', cex=0.4 )


points( smooth.spline(D_BIG$day, log2(D_BIG$bw_md)), pch=20, col='blue', cex=0.5 )
points( smooth.spline(D_MED$day, log2(D_MED$bw_md)), pch=20, col='purple', cex=0.5 )
points( smooth.spline(D_SMALL$day, log2(D_SMALL$bw_md)), pch=20, col='red', cex=0.5 )


####### Set the pretty X-axis and Y-axis ############################
YearLabels=seq(from=2010,to=2014,by=1)
YearLocations=c(182,547,912,1278,1643)
axis(1,at=YearLocations,labels=YearLabels )

par(las=1)
lab <- seq(from=-5,to=10,by=1)
labels <- parse(text=paste("2^", lab, sep="") )
axis(2,at=lab,labels=labels )




####### Plot the best-fit lines ############################
Dfit <- subset( D_BIG, day>=547 )
first_fit_day <- min(Dfit$day)
fit_BIG <- lm( log2(bw_md) ~ day, data=Dfit )


# Add the best-fit line (with black square) for the advertised bandwidth
segments( min(Dfit$day), predict(fit_BIG, data.frame(day=min(Dfit$day))),
          max(Dfit$day), predict(fit_BIG, data.frame(day=max(Dfit$day))),
          col="black", lty=2, lwd=3 )
points( min(Dfit$day), predict(fit_BIG, data.frame(day=min(Dfit$day))), col="black", pch=15, cex=1.3)


####################################################################

Dfit <- subset( D_MED, day>=547 )
first_fit_day <- min(Dfit$day)
fit_MED <- lm( log2(bw_md) ~ day, data=Dfit )


# Add the best-fit line (with black square) for the advertised bandwidth
segments( min(Dfit$day), predict(fit_MED, data.frame(day=min(Dfit$day))),
          max(Dfit$day), predict(fit_MED, data.frame(day=max(Dfit$day))),
          col="black", lty=2, lwd=3 )
points( min(Dfit$day), predict(fit_MED, data.frame(day=min(Dfit$day))), col="black", pch=15, cex=1.3)


####################################################################

Dfit <- subset( D_SMALL, day>=547 )
fit_SMALL <- lm( log2(bw_md) ~ day, data=Dfit )

# Add the best-fit line (with black square) for the advertised bandwidth
segments( min(Dfit$day), predict(fit_SMALL, data.frame(day=min(Dfit$day))),
          max(Dfit$day), predict(fit_SMALL, data.frame(day=max(Dfit$day))),
          col="black", lty=2, lwd=3 )
points( min(Dfit$day), predict(fit_SMALL, data.frame(day=min(Dfit$day))), col="black", pch=15, cex=1.3)



legend_texts = c(
  expression(paste("5   MiB ", r^2, "=0.46")),
  expression(paste("1   MiB ", r^2, "=0.48")),
  expression(paste("50 KiB ", r^2, "=0.55"))  
)


legend( "topleft", legend=legend_texts, inset=0.03, pch=c(20,20), col=c('blue','purple','red') ) 


