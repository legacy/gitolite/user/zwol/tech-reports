\subsection{Performance while under attack}
\label{perf-dos-attacks}

There are several ways to mount a Denial of Service (DoS) attack against the
Tor network. This section is an overview of the range of possible ways the
network could be attacked to cause resource exhaustion.

The following is heavily inspired by the 2015 Tor technical report
``Denial-of-service attacks in Tor: Taxonomy and
defenses''~\cite{dos-techreport}. Section~\ref{dos} describes some recent
attacks on the Tor network.

\subsubsection{Types of DoS attacks}

\paragraph{CPU consumption} 

Most of Tor's running time is spent performing expensive cryptographic
computation. It requires a lot of processing power on the CPU (central
processing unit---the computer's main processor) to accomplish these tasks. If
an attacker can cause relays or clients to use a lot of processing power on
other things, it could render Tor unable to accomplish its real tasks.

\textbf{Countermeasures}:

\begin{itemize}
  \item Use more efficient cryptographic algorithms.
  \item Rewrite parts of the software so that more tasks can run on multiple
  processors at a time.
  \item Modify our protocols to be more resistant to attacks that use up
  processing power.  
  \item Improve the code that schedules data to be sent from relays so that one
  user's traffic is less likely to interfere with another's.  
  \item Instrument the code so that we can better detect abnormal patterns of
  processor use.
  \end{itemize}

\paragraph{Memory consumption} 

Tor relies on having adequate computer memory available to function well.
Exhausting the available memory on a server running a relay leads to
performance degradation and sometimes crashes.

\textbf{Countermeasures.} There are
many places in the Tor code that could benefit from memory optimization: using
smaller objects in memory, cleaning up more frequently, optimizing access to
shared or common data, and so on.

\paragraph{Disk space consumption} 

These days, we rarely see servers\textemdash or even
clients\textemdash with very low disk space, but disk space is still very
relevant for certain parts of Tor. For example, directory authorities store
various data about all of the relays in the network on disk and use it to
create the Tor consensus.

\textbf{Countermeasures.} Detect and respond to low disk space events. Tor may
not be the cause of available disk space becoming low, but it is important that
we ensure that Tor can keep running, even with no available space at all.

\paragraph{Bandwidth consumption} 

Attacks that disrupt Tor's ability to move data between the client, the relays
in a circuit, and a destination by consuming a lot of bandwidth are
dangerous and when combined with other attacks can lead to
de-anonymization~\cite{torta05,congestion-longpaths}.

The simple version of this attack is expensive to mount because it requires
the amount of bandwidth that the attacker is trying to consume on the
target. This could be enough to interfere with one relay or many relays.

However, there are amplification attacks that, at very low cost, can
force the target relay to use more bandwidth than the attacker expends.
These are rare, but high-impact~\cite{jansen2014sniper}.

\textbf{Countermeasures:}

\begin{itemize}

	\item Implement better cell scheduling. We currently use the KIST
	scheduler, which is better than the earlier simple round-robin design,
	but KIST aims to work well under normal conditions, and a scheduling
	approach that remains robust to intentional congestion remains an
	open research problem.

	\item Improve our bandwidth measurement system. There is currently work
	underway to replace our old, poorly maintained bandwidth scanner,
	TorFlow~\cite{mikeperry-hotpets09},
	with a new Simple Bandwidth Scanner (SBWS)~\cite{sbws}.
	PeerFlow~\cite{peerflow-popets17}, a system proposed in the academic
	literature, is a possibility for the future. 

	\item Create a new Tor circuit and migrate a client's traffic to it if
	performance on the old circuit is poor.
  
	\item Optimize the onion services protocol to avoid bandwidth consumption
	attacks. Proposal~255~\cite{prop-255}, which would allow load balancing an
	onion service across a pool of servers, is one possibility.

\end{itemize}

\paragraph{Network resources exhaustion} 

TCP connections are specified with a port and IP address. There are 65535
available ports. If an attacker opens, and keeps open, connections to most or
all of the ports on a relay, it will become unusable.

\textbf{Countermeasures.} We currently have denial of service countermeasures
in place in the Tor relay software that aim to prevent such an attack.
% XXX yeah? what are they?

We could also explore using UDP, another transport protocol that doesn't suffer from this potential problem.

It might also be possible to use IPv6, which has a much larger range of
addresses, to mitigate this problem.

\subsubsection{Future directions}

To improve Tor's resilience against Denial of Service attacks, we should favor
defenses that address multiple types of attacks simultaneously over defenses
that simply address one at a time.

Therefore, we should look for general solutions and process improvements,
rather than piecemeal responses to individual attacks as they are discovered.

The technical report cited at the beginning of this section lists a series of
recommendations that are still relevant today. However, it's worth highlighting
one strategy in particular that has increasingly become the focus of
our development work in the past year or so: modularity. Rewriting the
Tor code to separate different
functionality into different modules will allow much finer grained control of
how resources are used. It will also help prevent interference between modules.
